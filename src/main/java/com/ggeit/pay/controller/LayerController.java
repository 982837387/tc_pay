package com.ggeit.pay.controller;


import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayApiException;
import com.alipay.api.internal.util.AlipaySignature;
import com.ggeit.pay.config.ALIPayConstants;
import com.ggeit.pay.config.WXPayConstants;
import com.ggeit.pay.config.bz_SysConstants;
import com.ggeit.pay.impl.AliPayServiceImpl;
import com.ggeit.pay.impl.OrderService;
import com.ggeit.pay.impl.WxPayServiceImpl;
import com.ggeit.pay.impl.bz_HisServiceImpl;
import com.ggeit.pay.utils.GGitUtil;

import net.sf.json.JSONArray;


@Controller
@RequestMapping("/lay")

// 支持返回页面只能使用@Controller、不能使用@RestController
public class LayerController {

	private final static Logger logger = LoggerFactory.getLogger(LayerController.class);

	@Autowired
	private WxPayServiceImpl wxpayimpl;
	
	@Autowired
	private AliPayServiceImpl alipayimpl;
	
	@Autowired
	private bz_HisServiceImpl bz_hisserviceimpl;
	
	@Autowired
	private OrderService orderservice;
	

	/**
	 * http://localhost:8080/aggpay/lay/index?cfsb=00001&brid=00002
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(HttpServletRequest request, HttpServletResponse response,
			 Map<String, Object> map) throws Exception {
		//判断扫描二维码的app
		logger.info("----------layer index----------");
		String userAgent = request.getHeader("User-Agent").toLowerCase();
		logger.info("index userAgent= " + userAgent);
		
		//userAgent = "|alipayclient";

		if (userAgent.indexOf("micromessenger") > 0) {
			logger.info("----------微信支付----------");
			String url = wxpayimpl.Authorize();
			logger.info("url = " + url);
			return "redirect:" + url;
		} else if (userAgent.indexOf("alipayclient") > 0) {
			logger.info("----------阿里支付----------");
			String url = alipayimpl.Authorize();
			logger.info("url = " + url);
			return "redirect:" + url;
		} else {
			map.put("tips", "未匹配到APP类型");
			return "failure";// 用了html不能使用反斜杠如"/tips"
		}
	}

	@RequestMapping(value = "/wxcallback", method = RequestMethod.GET)
	public String doWxCallBack(HttpServletRequest request, HttpServletResponse response, Map<String, Object> map) throws Exception {
		logger.info("----------layer wxcallback----------");
		//------------网页授权认证回调获取code---------------------
		String code = request.getParameter("code");
		String openid = wxpayimpl.GetOpenid(code);
		logger.info("wxcallback code = " + code);
		logger.info("wxcallback openid= " + openid);
		String car_num = "";
		Map<String,Object> selectmap = new HashMap<String,Object>();
		selectmap.put("openid", openid);
		//根据openid查询carnum_info数据库
		 List<Map> carnumlist = orderservice.selectOrder("tc_car.selectByOpenid",selectmap);
	        logger.info("carnumlist = " + carnumlist);	
	        for (int i = 0; i < carnumlist.size(); i++) {
					HashMap temp = (HashMap) carnumlist.get(i);
					logger.info("temp = "+ temp);
					car_num = temp.get("car_num").toString();
					logger.info("carnum = " + car_num);
		        }
	        if(car_num != "") {	
	        	//map.put("tip", "请确认车牌号");
				map.put("car_num", car_num);
				map.put("openid", openid);
				return "WXshow_carnum";
			}else {
				//map.put("tip", "请输入车牌号");
				map.put("openid", openid);
				return "WXcar_num";
			}   
	}
	
	@RequestMapping(value = "/alicallback", method = RequestMethod.GET)
	public String doAliCallBack(HttpServletRequest request, HttpServletResponse response, Map<String, Object> map) throws Exception {
		logger.info("----------layer alicallback----------");
		//------------网页授权认证回调获取code---------------------
		String auth_code = request.getParameter("auth_code"); 
		String userid = alipayimpl.GetUserid(auth_code);
		logger.info("alicallback userid= " + userid);

		String car_num = "";
		Map<String,Object> selectmap = new HashMap<String,Object>();
		selectmap.put("openid", userid);
		List<Map> carnumlist = orderservice.selectOrder("tc_car.selectByOpenid",selectmap);
		logger.info("carnumlist = " + carnumlist);	
        for (int i = 0; i < carnumlist.size(); i++) {
				HashMap temp = (HashMap) carnumlist.get(i);
				logger.info("temp = "+ temp);
				car_num = temp.get("car_num").toString();
				logger.info("carnum = " + car_num);
	        }
		 if(car_num != "") {	
	        	//map.put("tip", "请确认车牌号");
				map.put("car_num", car_num);
				map.put("openid", userid);
				return "Alishow_carnum";
			}else {
				//map.put("tip", "请输入车牌号");
				map.put("openid", userid);
				logger.info("map=" + map);
				return "Alicar_num";
			}   
	}
	
	@RequestMapping(value = "/ali_notify" ,method = RequestMethod.POST)
	public void alinotify(HttpServletRequest request, HttpServletResponse response) throws IOException, AlipayApiException{
		logger.info("----------ali notify----------");
		Map<String, String> params = convertRequestParamsToMap(request); // 将异步通知中收到的待验证所有参数都存放到map中
		
		String paramsJson = JSON.toJSONString(params);
	    logger.info("支付宝回调，{}", paramsJson);
	    
	 // 调用SDK验证签名
        boolean signVerified = AlipaySignature.rsaCheckV1(params, ALIPayConstants.ALIPAY_PUBLIC_KEY,
        		ALIPayConstants.CHARSET, ALIPayConstants.SIGNTYPE);
        if (signVerified) {
            logger.info("支付宝回调签名认证成功");
           if(check(params)) {
        	   logger.info("处理插库操作");
        	   Map<String,Object> insertmap = new HashMap<String,Object>();
        	   insertmap.put("trade_status", params.get("trade_status"));
        	   insertmap.put("trade_no", params.get("trade_no"));
        	   insertmap.put("out_trade_no", params.get("out_trade_no"));
        	   insertmap.put("total_amount", params.get("total_amount"));
        	   Boolean notifyflag = orderservice.insert("payStatus.insert", insertmap);
        	   logger.info("insert trade_status:" + notifyflag);
        	   
           }   
        }
      //支付宝要求必须返回success，不然就会一直给你回调
        PrintWriter writer = null;
        writer = response.getWriter();
        writer.write("success"); // 一定要打印success
        writer.flush();
	}

	// 将request中的参数转换成Map
    private static Map<String, String> convertRequestParamsToMap(HttpServletRequest request) {
        Map<String, String> retMap = new HashMap<String, String>();

        Set<Entry<String, String[]>> entrySet = request.getParameterMap().entrySet();

        for (Entry<String, String[]> entry : entrySet) {
            String name = entry.getKey();
            String[] values = entry.getValue();
            int valLen = values.length;

            if (valLen == 1) {
                retMap.put(name, values[0]);
            } else if (valLen > 1) {
                StringBuilder sb = new StringBuilder();
                for (String val : values) {
                    sb.append(",").append(val);
                }
                retMap.put(name, sb.toString().substring(1));
            } else {
                retMap.put(name, "");
            }
        }

        return retMap;
    }
    
    /**
     * 1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
     * 2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
     * 3、校验通知中的seller_id（或者seller_email)是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email），
     * 4、验证app_id是否为该商户本身。上述1、2、3、4有任何一个验证不通过，则表明本次通知是异常通知，务必忽略。
     * 在上述验证通过后商户必须根据支付宝不同类型的业务通知，正确的进行不同的业务处理，并且过滤重复的通知结果数据。
     * 在支付宝的业务通知中，只有交易通知状态为TRADE_SUCCESS或TRADE_FINISHED时，支付宝才会认定为买家付款成功。
     * 
     * @param params
     * @throws AlipayApiException
     */
    private Boolean check(Map<String, String> params) throws AlipayApiException {      
        String TradeNo = params.get("trade_no");

        // 1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
        //取outtradeno、totalamount
        Map<String,Object> selectmap = new HashMap<String,Object>();
        selectmap.put("trade_no", TradeNo);
        List<Map> paystatusList = orderservice.selectOrder("payStatus.selectBytradeno",selectmap);
		logger.info("paystatusList = " + paystatusList);
		
		String outTradeNo = (String) paystatusList.get(0).get("out_trade_no");
		String total_amount = (String) paystatusList.get(0).get("total_amount");
		
		logger.info("outTradeNo = " + outTradeNo);
		logger.info("total_amount = " + total_amount);
		
        if (!outTradeNo.equals(params.get("out_trade_no"))) {
            throw new AlipayApiException("out_trade_no错误");
        }       

        // 2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
        if (!total_amount.equals(params.get("total_amount"))) {
            throw new AlipayApiException("error total_amount");
        }
		return true;
    }



}
